var gulp = require('gulp');
var less = require('gulp-less');
var del = require('del');


gulp.task('less', ['clean'], function () {
  gulp.src('./less/*.less')
    .pipe(less({}))
    .pipe(gulp.dest('./css'));
});

gulp.task('clean', function () {
	del('css');
});

gulp.task('watch', function () {
	gulp.watch('./less/*.less', ['less']);
});

gulp.task('default', ['less', 'watch']);
